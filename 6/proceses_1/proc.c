#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include "proceses.h"

void take_proces(){
	pid_t pid;
	
	if((pid=fork())==0) {
		printf(" ПОТОМОК: Мой PID: %d\n", getpid());
        printf(" ПОТОМОК: %d, я твой сын!\n", getppid());
	}else{
		printf("РОДИТЕЛЬ: Мой PID -- %d\n", getpid());
        printf("РОДИТЕЛЬ: %d, Я ТВОЙ ОТЕЦ\n!",pid);
        wait(NULL);
	}
  }
