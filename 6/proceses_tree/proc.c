#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include "proceses.h"

void take_tree_proces(){
	
	int mass[6] = {0};
	pid_t pid, pid2, pid3, pid4, pid5;
	
	if((pid=fork())==0) {
		mass[2] = getpid();
		printf(" Pid_3: %d\n", mass[2]);
        printf("Ppi_3: %d\n\n", getppid());
		if((pid2=fork())==0) {
			mass[3] = getpid();
			printf(" Pid_4: %d\n", mass[3]);
			printf("Ppid_4: %d\n\n", getppid());
		}else{
			if((pid5=fork())==0) {
				mass[4] = getpid();
				printf(" Pid_5: %d\n", mass[4]);
				printf("Ppid_5: %d\n\n", getppid());
			}else{
				wait(NULL);
			}
			wait(NULL);
		}
	}else{
		if((pid3=fork())==0) {
			mass[1] = getpid();
			printf(" Pid_2: %d\n", mass[1]);
			printf("Ppid_2: %d\n\n", getppid());
			if((pid4=fork())==0) {
				mass[5] = getpid();
				printf(" Pid_6: %d\n", mass[5]);
				printf("Ppid_6: %d\n\n", getppid());
			}else{
				wait(NULL);
			}
        }else{
			mass[0] = getpid();
			printf(" Pid_1: %d\n", mass[0]);
			printf("Ppid_1 (pid init): %d\n\n", getppid());
			wait(NULL);
		}
		wait(NULL);
	}
}
