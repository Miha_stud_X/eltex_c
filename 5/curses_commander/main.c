#include <signal.h>
#include <stdlib.h>
#include <ncurses.h>
#include "commander.h"

int main(int argc, char ** argv)
{	
    //старт работы интерфейса
    initscr();
    signal(SIGWINCH, sig_winch);
    start_color();
    refresh();
  
    char putch_1[255] = "/";//адрес окна 1
    char putch_2[255] = "/";//адрес окна 2
    int highlight;//номер подсвеченного элемента
    short number_wnd = 1;//номер активного окна (из 2ух)
    
	init_choices_start(putch_2,2);//создание второго окна и заполнение его
	refresh();
    
    while(1){//основной цикл
		if (number_wnd == 1){
			highlight = init_choices(putch_1, number_wnd);
		}else{
			highlight = init_choices(putch_2, number_wnd);
		}
		if (highlight<0){
			if (highlight==-3){
				refresh();
				//перерисовывем окна
				init_choices_start(putch_1,1);
				refresh();
				init_choices_start(putch_2,2);
				refresh();
				continue;
			}
			if (highlight==-2) break;
			if (highlight==-1){ 
				number_wnd*=-1;
				refresh();
			}
		} else refresh();
	}
    //завершение работы интерфейса
    endwin();
    exit(EXIT_SUCCESS);
}
