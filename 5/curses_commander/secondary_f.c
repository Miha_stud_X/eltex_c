#include <sys/ioctl.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include "commander.h"

void sig_winch(int signo){
    struct winsize size;
    ioctl(fileno(stdout), TIOCGWINSZ, (char *) &size);
    resizeterm(size.ws_row, size.ws_col);
}

void sortname_alfabit(file_ *uk, int n){
	file_ tmp;
	for(int j=0; j<n; j++){
		for(int i=0; i<n-1; i++){
			if (strcmp(uk[i].name, uk[i+1].name)>0){
				tmp = uk[i];
				uk[i] = uk[i+1];
				uk[i+1] = tmp;
			}
		}
	}
}

char *str_putch(char * putch){
	char *str;
	str = (char*)malloc(sizeof(char)*(strlen(putch)));
	memset(str, 0, strlen(str));
	strncat(str, putch, (strrchr(putch, '/')-putch));
	strcpy(putch, str); 
	free(str);
	return putch;
}

void error(char *str){
	int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
	curs_set(FALSE);
	WINDOW *error_wnd;
	init_pair(1, COLOR_BLACK, COLOR_RED);
	error_wnd = newwin(4, xMax/2, yMax/4, xMax/4);
	wbkgd(error_wnd, COLOR_PAIR(1));
	mvwprintw(error_wnd, 1,1, "%s", str);
    wrefresh(error_wnd);
    getch();
    delwin(error_wnd);
}
