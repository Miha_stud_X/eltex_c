typedef struct File_{
	char type;
	char *name;
} file_;

void sig_winch(int);
int menu(file_ *, int, short, char* );
void start(file_ *, int, char *, int);
int init_choices(char *, short);
void init_choices_start(char *, int);
void sortname_alfabit(file_ *, int);
char *str_putch(char *h);
void error(char *);
