#include <ncurses.h>
#include "commander.h"

void start(file_ *choices, int n_list, char* putch, int number_wnd){
	
	WINDOW *status_wnd, *list_wnd;//окна адреса и списка файлов
	
	//задаём цветовые пары
    init_pair(1, COLOR_CYAN, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLUE);
	
	//берём размер экрана
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
	int max_n_sring = yMax-5;
	
	//создаём одно из двух окон
	if (number_wnd==1){
		status_wnd = newwin(1, xMax/2, 0, 0);
		list_wnd = newwin(yMax-2, xMax/2, 1, 0);
	}else{
		status_wnd = newwin(1, xMax/2, 0, xMax/2);
		list_wnd = newwin(yMax-2, xMax/2, 1, xMax/2);	
	}
	wbkgd(status_wnd, COLOR_PAIR(1));
	wbkgd(list_wnd, COLOR_PAIR(2));
	wattron(status_wnd, A_BOLD);
	box(list_wnd, 0, 0);
	
	//отображаем адрес
	wprintw(status_wnd, "%s", putch);
    wrefresh(status_wnd);
	
	//печать списка
	for(int i = 0; i < n_list; i++)
	{
		if (choices[i].type=='/') wattron(list_wnd, A_BOLD);
		mvwprintw(list_wnd, i+1, 1, "%c%s", choices[i].type, choices[i].name);
		wattroff(list_wnd, A_BOLD);
		if (i>=max_n_sring) break;//ограничение размера окна
	}
	wrefresh(list_wnd);
	
	delwin(status_wnd);
    delwin(list_wnd);
    
    mvprintw(yMax-1,0,"F10:Exit\tTAB:WindowChanhe");
    refresh();
}
