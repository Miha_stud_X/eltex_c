#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/wait.h>
#include "commander.h"

int menu(file_ *choices, int n_list, short number_win, char* putch){
	curs_set(FALSE);//прячем курсор
	
	WINDOW *status_wnd, *list_wnd;//окна адреса и списка файлов
	int choice = 0;//код клавиши
	int highlight = 0;//номер подсвечиваемого пункта
	int scrol = 0;//переменная для скрола списка
	
	//задаём цветовые пары
    init_pair(1, COLOR_CYAN, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLUE);
    init_pair(3, COLOR_RED, COLOR_BLUE);
	
	//берём размер экрана
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
    int max_n_sring = yMax-5;
	
	//создаём одно из двух окон
	if (number_win==1){
		status_wnd = newwin(1, xMax/2, 0, 0);
		list_wnd = newwin(yMax-2, xMax/2, 1, 0);
	}else{
		status_wnd = newwin(1, xMax/2, 0, xMax/2);
		list_wnd = newwin(yMax-2, xMax/2, 1, xMax/2);	
	}
	wbkgd(status_wnd, COLOR_PAIR(1));
	wbkgd(list_wnd, COLOR_PAIR(2));
	wattron(status_wnd, A_BOLD);
	box(list_wnd, 0, 0);
	wrefresh(list_wnd);
		
    //для использования кнопочек
	keypad(list_wnd, true);
	 

	//печать списка и перемещение по нему
	while(1){
		scrol = (highlight > max_n_sring ? (highlight - max_n_sring) : 0); 
		for(int i = scrol; i < n_list; i++){
			if (choices[i].type=='/') wattron(list_wnd, A_BOLD);
			wmove(list_wnd, i+1-scrol, 1);
			wclrtoeol(list_wnd);
			box(list_wnd, 0, 0);
			if(i == highlight){
				wattron(list_wnd, A_REVERSE);
				wprintw(list_wnd, "%c%s", choices[i].type, choices[i].name);
				wattroff(list_wnd, A_REVERSE);
			}else
				wprintw(list_wnd, "%c%s", choices[i].type, choices[i].name);
			wattroff(list_wnd, A_BOLD);
			if (i >= (max_n_sring + scrol)) break;//ограничение размера окна
		}
		
		if (highlight<0){
			wrefresh(list_wnd);
			break;
		}	
		
		//отображаем адрес
		wmove(status_wnd,0,0);
		wclrtoeol(status_wnd);
		mvwprintw(status_wnd,0,0, "%s/%s", putch, choices[highlight].name);
		wrefresh(status_wnd);

		choice = wgetch(list_wnd);
		
		//открытие директории
		if (choice == 10 && choices[highlight].type == '/')
			break;
		
		//запуск
		if (choice == 10 && choices[highlight].type == '*'){
			chdir(putch);
			pid_t pid;
			if ((pid = fork()) == 0){
				execl(choices[highlight].name,"", NULL);
			}else{
				wait(NULL);
			}
			highlight=-3;
			break;
		}
		
		switch(choice){
			case KEY_UP:
				highlight--;
				if (highlight == -1)
					highlight = 0;
				break;
			case KEY_DOWN:
				highlight++;
				if (highlight == n_list)
					highlight = n_list-1;
				break;
			case 0x9: // Tab
				highlight =-1;
				wmove(status_wnd,0,0);
				for (int k = 0; k<xMax/2; k++)
					wprintw(status_wnd, " ");
				mvwprintw(status_wnd,0,0, "%s", putch);
				wrefresh(status_wnd);
				break;
			case KEY_F(10): // F10 (выход)
				highlight =-2;
				break;
			default:
				break;			
		}
	}
	
	delwin(status_wnd);
    delwin(list_wnd);
	
	return highlight;
}

void init_choices_start(char *putch, int number_wnd){
	file_ *choices;
	int n = 0;
	DIR *dir;
	struct dirent *ent;
	
	if ((dir = opendir(putch)) != NULL) {
		while ((ent = readdir (dir)) != NULL) {
			if (strcmp(ent->d_name,".")){
				choices = (file_ *)realloc(choices, (n+1)*sizeof(file_));
				choices[n].name = (char*)malloc(sizeof(char)*(strlen(ent->d_name) + 1));
				strcpy(choices[n].name, ent->d_name);
				switch (ent->d_type){
					case DT_DIR:
						choices[n].type = '/';
						break;
					case DT_REG:
						if(choices[n].name[0]!='.')
							choices[n].type = '*';
						else choices[n].type = ' ';
						break;
					default:
						choices[n].type = ' ';
						break;
				}
				n++;
			}
		}
		closedir (dir);
		sortname_alfabit(choices, n);//сортируем список по алфавиту
		start(choices, n, putch, number_wnd);//отрисовываем меню окна при старте
	} else {
		error("Can't open directory\nPress any key to continue...");
	}
    
    for (;n>0;n--)
		free(choices[n-1].name);
    free(choices);
}

int init_choices(char *putch, short number_wnd){
	file_ *choices;
	int n = 0;
	int highlight;
	DIR *dir;
	struct dirent *ent;
	
	if ((dir = opendir(putch)) != NULL) {
		while ((ent = readdir (dir)) != NULL) {
			if (strcmp(ent->d_name,".")){
				choices = (file_ *)realloc(choices, (n+1)*sizeof(file_));
				choices[n].name = (char*)malloc(sizeof(char)*(strlen(ent->d_name) + 1));
				strcpy(choices[n].name, ent->d_name);
				switch (ent->d_type){
					case DT_DIR:
						choices[n].type = '/';
						break;
					case DT_REG:
						choices[n].type = '*';
						break;
					default:
						choices[n].type = ' ';
						break;
				}
				n++;
			}
		}
		closedir (dir);
		sortname_alfabit(choices, n);//сортируем список по алфавиту
		highlight = menu(choices, n, number_wnd, putch);//запускаем меню и ждём действий пользователя
	} else {
		error("Can't open directory\nPress any key to continue...");
		strcpy(putch,"/");
		highlight = -3;
    }
    
    
    if (highlight>=0){
		//putch = realpath(choices[highlight].name);
		if (strcmp(choices[highlight].name,"..")){//подъём(выход из текущей директории)
			strncat(putch, "/",1);
			strcat(putch, choices[highlight].name);
		}else{
			putch = str_putch(putch);//спуск(открытие директории)
		}
		
	}
	
	for (;n>0;n--)
		free(choices[n-1].name);
    free(choices);
    
    return highlight;
}
