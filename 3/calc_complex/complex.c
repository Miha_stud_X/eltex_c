#include <stdio.h>
#include "calc.h"

void print_complex(complex R){
	if (R.b>=0) printf("Результат:\n%.2f+%.2fi\n", R.a, R.b);
	else printf("Результат:\n%.2f-%.2fi\n", R.a, R.b);
}
