#include "calc.h"

complex del (complex A, complex B){
	complex R;
	float d = B.a*B.a+B.b*B.b;
	R.a=(A.a*B.a+A.b*B.b)/d;
	R.b=(A.b*B.a-A.a*B.b)/d;
	return R;
}
