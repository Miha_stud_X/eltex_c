#include <stdio.h>
#include "calc.h"


int main(){
	complex A;
	complex B;
	char c;
	while (1){
		printf("Введите 1ое комплексное число в алгебраической форме\n *+*i\n");
		scanf("%f+%fi", &(A.a), &(A.b));
		printf("Введите 2ое комплексное число в алгебраической форме\n *+*i\n");
		scanf("%f+%fi", &(B.a), &(B.b));
		while (1){
			getchar();
			printf("Введите действие (+,-,*,/,e)\n");
			scanf("%c", &c);
			if (c=='e') break;
			else
			switch (c){
				case '+': print_complex(plus(A,B)); break;
				case '-': print_complex(plus(A,inversion(B))); break;
				case '*': print_complex(multiplication(A,B)); break;
				case '/': print_complex(del(A,B)); break;
				default: printf("Введён неверный символ"); break;
			}
		}
		getchar();
		printf("Хотите ввести ещё числа? (Y or N)\n");
		scanf("%c", &c);
		if (c=='n' || c=='N') break;
	}
}
