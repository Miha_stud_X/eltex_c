#include <stdio.h>

int min(int a, int b, int c, int d){
	return ((a<b && a<c && a<d) ? a : ((b<c && b<d) ? d : (d<c ? d : c)));
}

//заполнение матрицы
void init_mass(int *mass, int n){
	
	/*------------Алгоритм по спирали (по значению)-------------------*/
	
	int k = 0, i = 0, j = -1, h = 1, s=n;
	while(k<n*n){
		 for (int f = 0; f<s; f++){
			 j+=h;
			 *(mass+i*n+j)=++k;
		 }
		 for (int f = 1; f<s; f++){
			 i+=h;
			 *(mass+i*n+j)=++k;
		 }
		 h*=-1;
		 s--;
	}
	
	/*------------Алгоритм построчно (по индексу)---------------------*/
	/*
	int r, p;
	for (int i = 0; i<n; i++)
		for (int j = 0; j<n; j++){
			r = min(n-1-j, j, n-1-i, i); //расстояние до края
			p = 4 * (n-r-1);//периметр
			*(mass+i*n+j) = (i>j?(p-(i+j)+1):(i+j+1))+(p+2)*r;
		}
	*/
}

//вывод матрицы
void print_mass(int *mass, int n){
	printf("\nРезультат:\n");
	for (int i = 0; i<n; i++){
		for (int j = 0; j<n; j++){
			printf("%d\t", *(mass+i*n+j));
		}
		printf("\n");
	}
}
