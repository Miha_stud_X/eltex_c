#include "spiral.h"
#include <stdio.h>

int main(){	
	int n;
	printf("Введите размер матрицы: ");
	scanf("%d", &n);
	int mass[n][n];
	//заполнение матрицы
	init_mass(&mass[0][0], n);
	//вывод матрицы
	print_mass(&mass[0][0], n);
}
