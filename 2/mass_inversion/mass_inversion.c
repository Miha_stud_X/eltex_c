#include <stdio.h>

//отражение массива
void inversion_mass(int *mass, int n){
	int tmp;
	for (int i = 0; i<(n/2); i++){
		tmp = *(mass+i);
		*(mass+i) = *(mass+n-i-1);
		*(mass+n-i-1) = tmp;
	}
}

//вывод массива
void print_mass(int *mass, int n){
	for (int i = 0; i<n; i++){
		printf("%d ", *(mass+i));	
	}
}
