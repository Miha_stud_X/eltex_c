#include "mass_inversion.h"
#include <stdio.h>

int main(){	
	int mass[10] = {1,2,3,4,5,6,7,8,9,10};
	//вывод исходного массива
	printf("\nИсходный массив:\n");
	print_mass(mass, 10);
	//выполнение отражения массива
	inversion_mass(mass, 10);
	//вывод отражённого массива
	printf("\nИнвертированный массив:\n");
	print_mass(mass, 10);
}
