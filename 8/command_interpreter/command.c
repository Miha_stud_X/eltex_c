#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <pthread.h> 
#include "command.h"

#define STR_SIZE 1024

char ** pars_str(char *str_old, int *f){
	char sep [10]="|";
	char *buf;
	char **str_new = NULL;
	int i=0;
	str_new = (char **)malloc(sizeof(char*));
	buf = strtok(str_old,sep);
	*str_new = (char*)malloc(sizeof(char)*(strlen(buf) + 1));
	strcpy(*str_new,buf);
	buf = strtok (NULL, sep);
	while( buf != NULL){
		i++;
		str_new = (char **)realloc(str_new, (i+1)*sizeof(char*));
		str_new[i] = (char*)malloc(sizeof(char)*(strlen(buf) + 1));
		strcpy(str_new[i],buf);
		buf = strtok (NULL, sep);
	}
	/*
	for (int f = i; f>=0; f--){
		printf("%s\n", *(str_new+f));
	}
	*/
	*f = i;
	return str_new;
}


int start(){
	int pid;
	int res;
	int fd[2];
	int i;
	char str[STR_SIZE+1];
	char str2[STR_SIZE+1];
	char **p_str;
	printf("Start command interpeter\n\n$: ");
	while(1){
		scanf("%1023[^\n]", str);
		p_str = pars_str(str, &i);
	
		for(int f = 0; f<i; f++){
			pipe(fd);
			if ((pid = fork())==0){
				dup2(fd[1], 1);
				strcpy(str, p_str[f]);
				execl("/bin/sh", "/bin/sh", "-c", str, NULL);
				//pthread_exit(NULL);
			}else{
				res = read(fd[0], str2, STR_SIZE/2);
				str2[res] = 0;
				wait(NULL);
				if (f<i){
					strcpy(str, p_str[f+1]);
					strncat(str, str2, STR_SIZE);
				}else{
					printf("$: %s", str2);
					printf("$: ");
					getchar();
				}
			}
		}
	}
	return 1;
}
