#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "pipe_link.h"

#define STR_SIZE 255
#define NAME_FILE "./../file_test"

int take_link(){
	
	int fd;
    char buf[]="THIS IS A TEST TEXT MKFIFO PIPE CHANEL";

    fd = open(NAME_FILE, O_RDWR);
    printf("Канал создан через файл \"%s\"\n", NAME_FILE);
	write(fd, buf, STR_SIZE-1);
    close(fd);
    return 0;
}
