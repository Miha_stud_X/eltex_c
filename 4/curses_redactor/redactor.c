#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <ncurses.h>
#include "redactor.h"

short redach(char *name){
	
	curs_set(TRUE);
    WINDOW *text_wnd, *box_wnd;
	int button = 0;//код клавиши
	short exit_ = 0;
	
	//задаём цветовые пары
    init_pair(1, COLOR_CYAN, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLUE);
    init_pair(3, COLOR_BLACK, COLOR_RED);
	
	//берём размер экрана
    int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
    
	//создаём окно
	box_wnd = newwin(yMax-1, xMax, 1, 0);
	text_wnd = newwin(yMax-3, xMax-2, 2, 1);
	wbkgd(box_wnd, COLOR_PAIR(2));
	wbkgd(text_wnd, COLOR_PAIR(1));
	box(box_wnd, 0, 0);
	wrefresh(box_wnd);
	
	//считываем строки из файла и выводим
	if (strcmp(name,"start")){	
		/*char arr[MAX_STRING_LEN];
		for(int i = 0; fgets(arr, MAX_STRING_LEN, file) != NULL; i++){
			mvwprintw(wnd, i+1, 1,"%s", arr);
		}*/
		FILE *file;
		if ((file = fopen(name, "r"))!=NULL){
			text_wnd = getwin(file);
			fclose(file);
		}else{
			error("Could not open file");
		}
		free(name);
	}
		
    
	keypad(text_wnd, true);//для использования кнопочек
	int xLoc = 0;
	int yLoc = 0;
	wmove(text_wnd,yLoc, xLoc);
	wrefresh(text_wnd);

	while(1){
		button = wgetch(text_wnd);	
		switch(button){
			case KEY_UP:
				if (yLoc>0)
					yLoc--;
				break;
			case KEY_DOWN:
				if (yLoc<yMax-4)
					yLoc++;
				break;
			case KEY_LEFT:
				if (xLoc>0)
					xLoc--;
				else 
					if (yLoc>0){
						xLoc = xMax-3;
						yLoc--;
					}
				break;
			case KEY_RIGHT:
				if (xLoc<xMax-3)
					xLoc++;
				else 
					if (yLoc<yMax-4){
						xLoc = 0;
						yLoc++;
					}
				break;
			case KEY_F(2): // F2 (Открыть)
				exit_ = 1;
				break;
			case KEY_F(3): // F3 (Сохранить)
				if (save(text_wnd)){
					exit_ = 1;
				}else{
					exit_ = 3;
				}
				break;
			case KEY_F(10): // F10 (Выйти)
				exit_ = 2;
				break;
			case KEY_BACKSPACE:
				if (xLoc>0)
					mvwaddch(text_wnd, yLoc, --xLoc, ' ');
				else 
					if (yLoc>0){
						xLoc = xMax-3;
						yLoc--;
					}
				break;
			default:
				wmove(text_wnd, yLoc, ++xLoc);
				break;			
		}
		if (exit_>0) break;
		wmove(text_wnd, yLoc, xLoc);
		wrefresh(text_wnd);
	}	
	delwin(text_wnd);
	return exit_;
}
