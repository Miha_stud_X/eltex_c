#include <sys/ioctl.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include "redactor.h"

#define MAX_NAME_LEN 15

void sig_winch(int signo){
    struct winsize size;
    ioctl(fileno(stdout), TIOCGWINSZ, (char *) &size);
    resizeterm(size.ws_row, size.ws_col);
}

char *enter(char *str){
	char *name, buf[MAX_NAME_LEN + 1];
	name = (char *)malloc(sizeof(char)*(strlen(buf)));
	int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
	curs_set(TRUE);
	WINDOW *enter_wnd;
	enter_wnd = newwin(5, xMax/2, yMax/4, xMax/4);	
	wbkgd(enter_wnd, COLOR_PAIR(3));
	mvwprintw(enter_wnd, 1,1, "%s\n", str);
	box(enter_wnd, 0, 0);
	wmove(enter_wnd, 2,3);
    wrefresh(enter_wnd);
    wgetnstr(enter_wnd, name, MAX_NAME_LEN);
    name[MAX_NAME_LEN] = 0;
    delwin(enter_wnd); 
    return name;
}

void error(char *str){
	int yMax, xMax;
    getmaxyx(stdscr, yMax, xMax);
	curs_set(FALSE);
	WINDOW *error_wnd;
	error_wnd = newwin(4, xMax/2, yMax/4, xMax/4);
	wbkgd(error_wnd, COLOR_PAIR(3));
	mvwprintw(error_wnd, 1,1, "%s", str);
	mvwprintw(error_wnd, 2,1, "Press any key to continue...");
	box(error_wnd, 0, 0);
    wrefresh(error_wnd);
    getch();
    delwin(error_wnd);
}


short open(){
	char * name;
	name = enter("Enter file name to open");
	return redach(name);
}

short save(WINDOW* wnd){
	char * name;
	name = enter("Enter file name to save");
	return write_f(wnd, name);
}

int write_f(WINDOW *wnd, char *name){
	
	FILE *file;
	
	if ((file = fopen(name, "w"))!=NULL){
		putwin(wnd, file);
		fclose(file);
	}else{
		error("Could not save file");
		return 1;
	}
	return 0;
}

