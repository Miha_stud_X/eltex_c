#include <signal.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include "redactor.h"

int main(int argc, char ** argv)
{	
	//старт работы интерфейса
    initscr();
    signal(SIGWINCH, sig_winch);
    start_color();
    printw("\tF2: Open\tF3: Save\tF10: Exit");
    refresh();
	short n;
	
	if (argc>1){
		char *name = (char *)malloc(sizeof(char)*(strlen(argv[1])));
		strcpy(name,argv[1]);
		n = redach(name);
	}else{
		n = redach("start");
	}
	
	while (1){
		if (n==2) break;
		else{
		if (n==1) n = open();
		if (n==3) n = redach("start");
		}
	}
	//завершение работы интерфейса
    endwin();
    exit(EXIT_SUCCESS);
}
