#include <stdio.h>
#include <pthread.h>
#include <unistd.h> 
#include "potocs.h"

#define number_buyers 3
#define number_stores 5

pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;

typedef struct Store{
	int count;
	short empl;
} store;

typedef struct Buy_man{
	int number;
	int needs;
} buy_man;

store stores [5];

void *buyer(void *ptc){
	int i = 0;
	buy_man *ptc1 = ptc;
	while(ptc1->needs > 0){
		while(1){
			if (i<number_stores){
				if (stores[i].empl == 1 || stores[i].count == 0) i++;
				else break;
			}else{ 
				i=0;
			}
		}
		pthread_mutex_lock(&mut);//
		stores[i].empl = 1;
		pthread_mutex_unlock(&mut);//
		if (stores[i].count<ptc1->needs){
			ptc1->needs-=stores[i].count;
			stores[i].count=0;
		}else{
			stores[i].count-=ptc1->needs;
			ptc1->needs=0;
		}
		stores[i].empl = 0;
		printf("Покупатель №%d:  В магазине %d, Потребность: %d\n", ptc1->number, i+1, ptc1->needs);
	}
	return 0;
}

void *loader(void *ptc){
	int *ptc1 = ptc;
	int i = 0;
	while (*ptc1 < (10000*number_buyers)){
		while(1){
			if (i<number_stores){
				if (stores[i].empl == 1) i++;
				else break;
			}else{ 
				i=0;
			}
		}
		pthread_mutex_lock(&mut);//
		stores[i].empl = 1;
		pthread_mutex_unlock(&mut);//
		stores[i].count+=500;
		stores[i].empl = 0;
		*ptc1+=500;
		printf("Погрузчик добавил в магазин %d товар: %d\n", i+1, stores[i].count);
		i++;
		sleep(2);
	}
	return 0;
}


int take_potocs(){
	
	pthread_t tid[4];
	buy_man mans[3];
	int sum_start = 0;
	
	for (int i = 0; i<number_buyers; i++){
		mans[i].needs = 10000;
		mans[i].number = i+1;
	}
	
	for (int i = 0; i<number_stores; i++){
		stores[i].empl = 0;
		stores[i].count = 1000;
		sum_start+=stores[i].count;
	}
	
	for (int i = 0; i<number_buyers; i++){
		pthread_create(&tid[i], NULL, buyer, &mans[i]);
	}
	pthread_create(&tid[3], NULL, loader, &sum_start);
	
	for (int i = 0; i<4; i++){
		pthread_join(tid[i], NULL);
	}
	
	return 0;
}
