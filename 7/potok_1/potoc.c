#include <stdio.h>
#include <pthread.h>
#include "potocs.h"

void *func(void *ptc){
	int *ptc1 = ptc;
	printf("%d\n",*ptc1);
	return 0;
}

int take_potocs(){
	
	pthread_t tid[5];
	int mass[5] = {1,2,3,4,5};
	
	for (int i = 0; i<5; i++){
		pthread_create(&tid[i], NULL, func, &mass[i]);
	}
	
	for (int i = 0; i<5; i++){
		pthread_join(tid[i], NULL);
	}
	
	return 0;
}
